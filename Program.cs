﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GettingFormsUp
{
    static class Program
    {
        private static bool hideFlag = true;

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var initApplicationContext = new InitApplicationContext();

            Console.WriteLine();
            var fileSystemWatcher = new FileSystemWatcher(Path.GetFullPath("../.."), "watcher");
            fileSystemWatcher.Changed += (sender, args) =>
            {
                Console.WriteLine("Watched");
                initApplicationContext.Show();
            };
            fileSystemWatcher.EnableRaisingEvents = true;

            Application.Run(initApplicationContext);
        }

        private class InitApplicationContext : ApplicationContext
        {
            private static MainWindow _mainWindow;

            public InitApplicationContext()
            {
                NewForm();
            }

            private void NewForm()
            {
                Console.WriteLine("Creating new MainWindow");
                _mainWindow = new MainWindow();
                _mainWindow.Invoke((MethodInvoker) delegate
                {
                    _mainWindow.Show(); 
                });
            }

            public void Show()
            {
                if (_mainWindow == null || _mainWindow.IsDisposed)
                {
                    NewForm();
                }
                else if (!_mainWindow.Visible)
                {
                    _mainWindow.BeginInvoke((MethodInvoker) delegate
                    {
                        Console.WriteLine("showing");
                        _mainWindow.Show();
                    });
                }
            }

            public void Delete()
            {
                if (_mainWindow != null && !_mainWindow.IsDisposed)
                {
                    _mainWindow.Dispose();
                }
            }
        }
    }
}